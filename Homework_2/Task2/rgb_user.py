#!/usr/bin/env python3
import rospy
from std_msgs.msg import Int8

cmds_codes = {"r": 0, "g": 1, "b": 2}


def main():
    pub = rospy.Publisher("/computer/traffic_light/cmd", Int8, queue_size=10)
    rospy.init_node("rgb_user", anonymous=False)
    rate = rospy.Rate(10)

    while not rospy.is_shutdown():
        user_input = str(input("Command (r, g, b) - ")).lower()

        if user_input in cmds_codes.keys():
            pub.publish(cmds_codes[user_input])
            rate.sleep()


if __name__ == "__main__":
    main()