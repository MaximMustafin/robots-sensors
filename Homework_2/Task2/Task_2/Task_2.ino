#include <ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Int8.h>
#include <string.h>

int red_led = 6;
int green_led = 5;
int blue_led = 4;

int cmd = -1;

void callback(const std_msgs::Int8 &msg)
{
  cmd = msg.data;
}

// ROS
ros::NodeHandle  nh;
std_msgs::String rgbState_msg;
ros::Publisher pub_rgbState("/arduino/traffic_light/status", &rgbState_msg);
ros::Subscriber<std_msgs::Int8> sub("computer/traffic_light/cmd", &callback);

void setup()
{
  // ROS
  nh.initNode();
  nh.advertise(pub_rgbState);
  nh.subscribe(sub);

  // Serial.begin(9600);

  pinMode(red_led, OUTPUT);
  pinMode(green_led, OUTPUT);
  pinMode(blue_led, OUTPUT);
}

void loop()
{
  if(cmd == 0)
  {
    digitalWrite(red_led, HIGH);
    digitalWrite(green_led, LOW);
    digitalWrite(blue_led, LOW);
    rgbState_msg.data = "Red";
  }
  else if(cmd == 1)
  {
    digitalWrite(red_led, LOW);
    digitalWrite(green_led, HIGH);
    digitalWrite(blue_led, LOW);
    rgbState_msg.data = "Green";
  }
  else if(cmd == 2)
  {
    digitalWrite(red_led, LOW);
    digitalWrite(green_led, LOW);
    digitalWrite(blue_led, HIGH);
    rgbState_msg.data = "Blue";
  }

  pub_rgbState.publish(&rgbState_msg);

  nh.spinOnce();
  delay(1000);
}
