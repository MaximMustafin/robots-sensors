#include <ros.h>
#include <std_msgs/Bool.h>

int led = 6;
volatile boolean interruptFlag = false;
volatile boolean ledEnabled = false;

// ROS
ros::NodeHandle  nh;
std_msgs::Bool ledState_msg;
ros::Publisher pub_ledState("/arduino/led", &ledState_msg);

void setup()
{
  // ROS
  nh.initNode();
  nh.advertise(pub_ledState);

  pinMode(led, OUTPUT);
  attachInterrupt(0, buttonTick, FALLING);
}

void buttonTick(){
  interruptFlag = true;
}

void loop()
{
  ledState_msg.data = ledEnabled;
  pub_ledState.publish(&ledState_msg);
  nh.spinOnce();
  delay(1000);

  if(!interruptFlag){
    return;
  }

  interruptFlag = false;
  ledEnabled = !ledEnabled;
  digitalWrite(led, ledEnabled);
}
