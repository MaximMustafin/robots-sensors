#!/usr/bin/env python3
import rospy
from std_msgs.msg import Int8

cmds_codes = {"w": 0, "a": 1, "s": 2, "d": 3}


def main():
    pub = rospy.Publisher("/computer/lcd/cmd", Int8, queue_size=10)
    rospy.init_node("dot_user", anonymous=False)
    rate = rospy.Rate(10)

    while not rospy.is_shutdown():
        user_input = str(input("Command (w, a, s, d) - ")).lower()

        if user_input in cmds_codes.keys():
            pub.publish(cmds_codes[user_input])
            rate.sleep()


if __name__ == "__main__":
    main()