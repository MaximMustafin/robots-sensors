#include  <LiquidCrystal.h>
#include <ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Int8.h>

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

int cmd = -1;

void callback(const std_msgs::Int8 &msg)
{
  cmd = msg.data;
}

// ROS
ros::NodeHandle nh;
ros::Subscriber<std_msgs::Int8> sub("computer/lcd/cmd", &callback);

int min_x = 0;
int max_x = 15;
int min_y = 0;
int max_y = 1;

int curr_x = 0;
int curr_y = 0;

int prev_x = 0;
int prev_y = 0;
 
void setup()
{
  // ROS
  nh.initNode();
  nh.subscribe(sub);

  lcd.begin(16, 2);
  delay(500);
}
void loop()
{
  if(cmd == 0)
  {
    curr_y++;
  }
  else if(cmd == 1)
  {
    curr_x--;
  }
  else if(cmd == 2)
  {
    curr_y--;
  }
  else if(cmd == 3)
  {
    curr_x++;
  }

  if(curr_x > max_x) curr_x = min_x;
  else if(curr_x < min_x) curr_x = max_x;

  if(curr_y > max_y) curr_y = min_y;
  else if(curr_y < min_y) curr_y = max_y;

  if(prev_x != curr_x || prev_y != curr_y)
  {
    lcd.begin(16, 2);
    prev_x = curr_x;
    prev_y = curr_y;
  }

  cmd = -1;
  lcd.setCursor(curr_x, curr_y);
  lcd.print(".");

  nh.spinOnce();
}