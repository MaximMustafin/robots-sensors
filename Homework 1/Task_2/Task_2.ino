int led = 6;
volatile boolean interruptFlag = false;
volatile boolean ledEnabled = false;

void setup() {
  Serial.begin(9600);
  pinMode(led, OUTPUT);
  attachInterrupt(0, buttonTick, FALLING);
}

void buttonTick(){
  interruptFlag = true;
}

void loop() {
  if(!interruptFlag){
    return;
  }

  interruptFlag = false;
  ledEnabled = !ledEnabled;
  digitalWrite(led, ledEnabled);
  ledEnabled ? Serial.println("LED: on") : Serial.println("LED: off");
}
