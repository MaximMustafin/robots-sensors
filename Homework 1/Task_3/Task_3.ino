#define LED_PIN 6
#define CLK 7
#define DT 3
// #define SW 4 

int currentStateCLK;
int lastStateCLK;
int brightness = 0;
int fadeValue = 7;

void setup() {
    pinMode(LED_PIN, OUTPUT);
    pinMode(CLK, INPUT);
    pinMode(DT, INPUT);
    // pinMode(SW, INPUT_PULLUP);
    Serial.begin(9600); 
    lastStateCLK = digitalRead(CLK);
}

void loop() {
  currentStateCLK = digitalRead(CLK);

  if (currentStateCLK != lastStateCLK  && currentStateCLK == 1){
    if (digitalRead(DT) != currentStateCLK) {
      brightness += fadeValue;

    } else {
      brightness -= fadeValue;
    }
    
    Serial.print("Brightness: ");
    Serial.println(constrain(brightness, 0, 255));
  }

  lastStateCLK = currentStateCLK;             

  brightness = constrain(brightness, 0, 255);
  analogWrite(LED_PIN, brightness);
}