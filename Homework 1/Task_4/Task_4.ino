#define LED_PIN 6
#define CLK 7
#define DT 3

int currentStateCLK;
int lastStateCLK;
int brightness = 0;
int fadeValue = 7;

volatile boolean interruptFlag = false;
volatile boolean ledEnabled = false;

void setup() {
    pinMode(LED_PIN, OUTPUT);
    pinMode(CLK, INPUT);
    pinMode(DT, INPUT);
    // pinMode(SW, INPUT_PULLUP);
    Serial.begin(9600); 
    lastStateCLK = digitalRead(CLK);

    attachInterrupt(0, buttonTick, FALLING);
}

void buttonTick(){
  interruptFlag = !interruptFlag;
}

void loop() {
  if(interruptFlag){
    ledEnabled = !ledEnabled;
    ledEnabled ? Serial.println("LED: on") : Serial.println("LED: off");
    interruptFlag = false;
  }

  if(!ledEnabled){
    analogWrite(LED_PIN, 0);
    return;
  }
  currentStateCLK = digitalRead(CLK);

  if (currentStateCLK != lastStateCLK  && currentStateCLK == 1){
    if (digitalRead(DT) != currentStateCLK) {
      brightness += fadeValue;

    } else {
      brightness -= fadeValue;
    }
    
    Serial.print("Brightness: ");
    Serial.println(constrain(brightness, 0, 255));
  }

  lastStateCLK = currentStateCLK;             

  brightness = constrain(brightness, 0, 255);
  analogWrite(LED_PIN, brightness);
}