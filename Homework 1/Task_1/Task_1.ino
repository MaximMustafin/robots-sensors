int led = 6;
int hz = 1;
float T = 0;

void setup() {
  pinMode(led, OUTPUT);
  hz != 0 ? T = 1000/abs(hz) : T = 0;
}

void loop() {
  if(T == 0) return;

  analogWrite(led, 255);
  delay(T/2);

  analogWrite(led, 0);
  delay(T/2);
}
