#!/usr/bin/env python3
import rospy
from sensor_msgs.msg import LaserScan
from typing import Final

MIN_RANGE: Final[float] = 0.12
MAX_RANGE: Final[float] = 7.0

INF: Final[float] = float('inf')
NAN: Final[float] = float('nan')

pub = rospy.Publisher("scan/filtered", LaserScan, queue_size=10)


def callback(msg: LaserScan):
	msg.ranges = tuple(map(lambda x: -1 if x == INF or x == NAN else x, msg.ranges))
	msg.ranges = tuple(map(lambda x: -1 if x < MIN_RANGE or x > MAX_RANGE else x, msg.ranges))
	pub.publish(msg)


def main():
	rospy.init_node("scan_filter")
	rospy.Subscriber("scan/raw", LaserScan, callback)
	rospy.spin()


if __name__ == '__main__':
	main()
