#!/usr/bin/env python3
import rospy
import pygame
import math

from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist

MIN_SCAN_ANGLE_DEGREES = 5
MAX_SCAN_ANGLE_DEGREES = 7

MIN_SCAN_ANGLE_RADIANS = MIN_SCAN_ANGLE_DEGREES/180.0*math.pi
MAX_SCAN_ANGLE_RADIANS = MAX_SCAN_ANGLE_DEGREES/180.0*math.pi

curr_distance = 0
curr_direction = 0


def twist_callback(twist: Twist):
    global curr_direction

    if twist.linear.x > 0:
        curr_direction = 1
    elif twist.linear.x < 0:
        curr_direction = -1
    elif twist.linear.x == 0:
        curr_direction = 0


def scan_callback(filtered_scan: LaserScan):
    global curr_distance

    min_index = round((MIN_SCAN_ANGLE_RADIANS - filtered_scan.angle_min) / filtered_scan.angle_increment)
    max_index = round((MAX_SCAN_ANGLE_RADIANS - filtered_scan.angle_min) / filtered_scan.angle_increment)

    ranges_sum = 0
    k = 0

    for i in range(min_index, max_index):
        if filtered_scan.ranges[i] == -1.0:
            continue

        ranges_sum += filtered_scan.ranges[i]
        k += 1

    distance = ranges_sum / k

    if abs(distance - curr_distance) > 0.025:
        curr_distance = distance


def main():
    rospy.init_node("distance_direction_calc")
    pygame.init()
    pygame.font.init()

    rospy.Subscriber("scan/filtered", LaserScan, scan_callback)
    rospy.Subscriber("cmd_vel", Twist, twist_callback)

    screen = pygame.display.set_mode((1280, 720))
    font = pygame.font.SysFont('Calibri', 40)
    stop_text = font.render('Stopped', False, (0, 0, 0))
    run = True
    
    while run and not rospy.is_shutdown():
        screen.fill((255, 255, 255))

        pygame.draw.circle(screen, (0, 0, 255), (300, 350), 70)
        pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(1000, 100, 50, 500))
        pygame.draw.rect(screen, (255, 0, 0), pygame.Rect(390, 350, 590, 5))

        distance_text = font.render(f'{curr_distance:.03f} m', False, (0, 0, 0))
        screen.blit(distance_text, (650, 300))

        if curr_direction == 1:
            pygame.draw.polygon(screen, (0, 255, 255), ((25+550,370),(25+550, 370+100),(200+550,(2*370+100)//2)))
        elif curr_direction == -1:
            pygame.draw.polygon(screen, (0, 255, 255), ((25+750,370),(25+750, 370+100),(600,(2*370+100)//2)))
        elif curr_direction == 0:
            screen.blit(stop_text, (650, 350))

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False

        pygame.display.flip()

    pygame.quit()

        
if __name__ == '__main__':
    main()
