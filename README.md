# Robots Sensors

## [+] Homework 1 - Светодиод
- [+] А. Мигающий светодиод
- [+] Б. Подключение кнопки
- [+] В. Подключение потенциометра (энкодера)
- [+] Г. Дополнительное задание: кнопка и потенциометр (энкодер)
- Videos - https://drive.google.com/drive/folders/1_mubTOFddaoX6vgoIMLOSWz9QG2uxvZy?usp=drive_link

## [+] Homework 2 - ROS Arduino
- [+] A. Интеграция Arduino с ROS
- [+] Б. Светодиодный светофор
- [+] В. ЖК дисплей
- Videos - https://drive.google.com/drive/folders/1CLyHfkiqKHyB9SCCpKvhg_Q7vJdT9iXz?usp=drive_link

## [+] Homework 3 - Sensor filtering
- [+] 1. Запустить симуляцию в Gazebo
- [+] 2. Реализовать ROS узел для фильтрации данных с дальномера
- [+] 3. Реализовать ROS узел для вычисления расстояния до препятствия и направления движения дальномера
- Video - https://drive.google.com/drive/folders/1U8A277a4Rsjww2UVoPFrXdsWpCV2qX_F?usp=sharing

## [+] Homework 4 - ПИД-регулятор
- [+] 1. Склонировать репозиторий.
- [+] 2. Собрать проект в соответствии с инструкциями в README.
- [+] 3. Реализовать ПИД-регулятор для управления мобильной платформой.
- Video - https://drive.google.com/drive/folders/1GmK2DvO3-Ph3Nb_fbLmT4JeaKfiXuJZW?usp=drive_link