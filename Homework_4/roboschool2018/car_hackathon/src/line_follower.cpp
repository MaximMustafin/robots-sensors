#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <car_msgs/MotorsControl.h>
#include <stdint.h>

void image_callback(const sensor_msgs::Image::ConstPtr& msg);
void motors(int16_t left, int16_t right);
int16_t truncate(int16_t pwm);

const uint16_t MAX_PWM = 255;

// {p: 100.0, i: 0.01, d: 10.0}
float Kp = 100;
float Ki = 0.01;
float Kd = 15;

float max_speed = 100;
float left_motor = 0;
float right_motor = 0;

float prev_err = 0;
int err_window_size = 1000;
int err_count = 0;
float err_sum = 0;


ros::Publisher pub;

int main(int argc, char** argv)
{
    ros::init(argc, argv, "line_follower");
    ros::NodeHandle nh;

    auto sub = nh.subscribe("/car_gazebo/camera1/image_raw", 5, image_callback);
    pub = nh.advertise<car_msgs::MotorsControl>("/motors_commands", 10);

    ros::spin();
    return 0;
}

float pid(float err)
{
    if(err_count < err_window_size) 
    {
        err_sum += err;
        err_count++;
    }
    else 
    {
        err_sum = err;
        err_count = 1;
    }

    return Kp * err + Ki * err_sum + Kd * abs(err - prev_err);
}

void image_callback(const sensor_msgs::ImageConstPtr& msg)
{        
    cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);

    cv::Mat image = cv_ptr->image;
    cv::Mat hsvImage;
    cv:cvtColor(image, hsvImage, CV_BGR2HSV);

    cv::Mat mask;
    cv::Scalar lower_red(0, 100, 20);
    cv::Scalar upper_red(10, 255, 255);
    cv::inRange(hsvImage, lower_red, upper_red, mask);

    int width = mask.cols;
    int height = mask.rows;

    int search_top = 3 * height / 4;
    int search_bottom = search_top + 100;

    for (int y = 0; y < height - 2; y++) {
        if (y < search_top || y > search_bottom) {
            for (int x = 0; x < width; x++) {
                mask.at<cv::Vec3b>(y, x)[0] = 0;
                mask.at<cv::Vec3b>(y, x)[1] = 0;
                mask.at<cv::Vec3b>(y, x)[2] = 0;
            }
        }
    }

    cv::Moments M = cv::moments(mask);

    if (M.m00 > 0) {
        int cx = int(M.m10 / M.m00);
        int cy = int(M.m01 / M.m00);
        cv::circle(image, cv::Point(cx, cy), 10, CV_RGB(0, 255, 0), -1);

        float err = float(cx - width / 2) / (width * 0.5);

        if(err < 0)
        {
            err = 1 - abs(err);
            left_motor = pid(err);
            right_motor = max_speed;
        }
        else if(err > 0)
        {
            err = 1 - abs(err);
            left_motor = max_speed;
            right_motor = pid(err);
        }

        prev_err = err;

        motors(left_motor, right_motor);
    }

    cv::imshow("image", image);
    cv::waitKey(1);
}

void motors(int16_t left, int16_t right)
{
    car_msgs::MotorsControl msg;
    msg.left = truncate(left);
    msg.right = truncate(right);
    pub.publish(msg);
}

int16_t truncate(int16_t pwm)
{
    if(pwm < -MAX_PWM)
        return -MAX_PWM;
    if(pwm > MAX_PWM)
        return MAX_PWM;
    return pwm;
}